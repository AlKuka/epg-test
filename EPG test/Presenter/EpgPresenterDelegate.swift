//
//  EpgPresenterDelegate.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation


protocol EpgPresenterDelegate {
    func loadChannels(result: @escaping ([Channel]?) -> Void)
    func loadEpg(result: @escaping ([ProgramItem]?) -> Void)
}
