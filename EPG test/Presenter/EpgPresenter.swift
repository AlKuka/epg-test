//
//  EpgPresenter.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation


class EpgPresenter {
    var delegate: EpgPresenterDelegate?
    var epgView: EPGViewControllerInput?
    
    private func loadChannels() {
        delegate?.loadChannels(result: { [self] data in
            if data != nil {
                epgView?.putChannels(data!)
                loadPrograms()
            } else {
                print("channels didn't load")
            }
        })
    }
    
    private func loadPrograms() {
        delegate?.loadEpg(result: { [self] data in
            if data != nil {
                epgView?.putPrograms(data!)
                epgView?.arrangeByChannels()
            } else {
                print("programs didn't load")
            }
        })
    }
}


extension EpgPresenter: EPGViewControllerDelegate {
    func channelDidSelect(id: Int) {
        print("channel did select")
    }
    
    func programItemDidSelect(id: Int) {
        print("program item did select")
    }
    
    func epgViewDidLoad() {
        loadChannels()
    }
}
