//
//  Channel_JSON.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 11.02.2023.
//

import Foundation


// MARK: - Структуры JSON
public struct Channel: Decodable {
    let orderNum : Int // order of the channel in the TV Guide
    let accessNum : Int // Channel access and display number
    let CallSign : String // Channel CallSIgn - shown in TV Guide
    let id : Int // channel ID
}

public struct RecentAirTime: Decodable {
    let id: Int // program item ID
    let channelID : Int // its channel ID
}

// describes Program Guide item
public struct ProgramItem: Decodable {
    let startTime : String // program item start time in UTC
    let recentAirTime : RecentAirTime // program item ID and channel ID the program belongs to
    let length : Int // program length in minutes
    let shortName : String?
    let name : String // Program name to display
}
