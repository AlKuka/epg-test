//
//  Common Resurces.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation


public let domain = "https://demo-c.cdn.vmedia.ca"

public enum UrlItems : String {
    case jsonChannels = "/json/Channels",
         jsonPrograms = "/json/ProgramItems"
}
