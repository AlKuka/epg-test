//
//  Network Service.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation


class NetworkService {
    
    private let session = URLSession.shared
    
    
    private func getUrl(item: UrlItems) -> URL {
        let address = domain + item.rawValue
        guard let url = URL(string: address) else {
            fatalError("incorrect URL address")
        }
        return url
    }
    
    
    func getData(item: UrlItems, result: @escaping (Data?) -> Void) {
        
        let request = URLRequest(url: getUrl(item: item))
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                if error != nil || response == nil {
                    print(error!.localizedDescription)
                    result(nil)
                } else {
                    result(data)
                }
            }
        })
            
        task.resume()
    }
}
