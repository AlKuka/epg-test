//
//  Server_Interactor.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 12.02.2023.
//

import Foundation


class ServerInteractor {
    
    private let networkService = NetworkService()
    
}


extension ServerInteractor: EpgPresenterDelegate {
    
    func loadChannels(result: @escaping ([Channel]?) -> Void) {
        networkService.getData(item: .jsonChannels) { data in
            if data != nil,
               let answer = try? JSONDecoder().decode([Channel].self, from: data!) {
                result(answer)
            } else {
                result(nil)
            }
        }
    }
    
    func loadEpg(result: @escaping ([ProgramItem]?) -> Void) {
        networkService.getData(item: .jsonPrograms) { data in
            if data != nil,
               let answer = try? JSONDecoder().decode([ProgramItem].self, from: data!) {
                result(answer)
            } else {
                result(nil)
            }
        }
    }
}
