//
//  EpgTableViewCell.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 13.02.2023.
//

import UIKit

class EpgTableViewCell: UITableViewCell {
    
    override var canBecomeFocused: Bool {
        return false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func clearAllItems() {
        for item in contentView.subviews.reversed() {
            item.removeFromSuperview()
        }
    }
}
