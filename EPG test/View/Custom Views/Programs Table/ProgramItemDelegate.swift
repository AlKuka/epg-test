//
//  ProgramItemDelegate.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation

protocol ProgramItemDelegate {
    func programItemIsFocused(id: Int)
}
