//
//  ProgramItemView.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 13.02.2023.
//

import UIKit

@IBDesignable
class ProgramItemView: UIView {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet var contentView: UIView!
    let kCONTENT_XIB_NAME = "ProgramItemView"
    
    var nameString = "empty"
    var timeString = "empty"
    var delegate : ProgramItemDelegate?
    var programId = 0
    
    //MARK: - Focus behavior
    override var canBecomeFocused: Bool {
        return true
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.previouslyFocusedView === self {
            UIView.animate(withDuration: 0.1, animations: {
                context.previouslyFocusedView?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.contentView.backgroundColor = .blue
            })
        }
        
        if context.nextFocusedView === self {
            UIView.animate(withDuration: 0.1, animations: { [self] () -> Void in
                context.nextFocusedView?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                contentView.backgroundColor = .systemBlue
                delegate?.programItemIsFocused(id: programId)
            })
        }
    }
    
    func setup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        contentView.backgroundColor = .blue
        addSubview(contentView)
        name.text = nameString
        time.text = timeString
        layoutIfNeeded()
        clipsToBounds = true
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        setup()
    }

    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: ProgramItemView.self)
        let view = bundle.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)?[0] as! UIView
        return view
    }
}
