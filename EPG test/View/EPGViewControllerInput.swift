//
//  EPGViewControllerInput.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation

protocol EPGViewControllerInput {
    func putChannels(_ data: [Channel])
    func putPrograms(_ data: [ProgramItem])
    func arrangeByChannels()
}
