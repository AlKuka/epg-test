//
//  EPGViewController.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 11.02.2023.
//

import UIKit

class EPGViewController: UIViewController {

    @IBOutlet weak var channelsTable: UITableView!
    @IBOutlet weak var epgScrollView: UIScrollView!
    @IBOutlet weak var epgTable: UITableView!
    @IBOutlet weak var epgTableWidht: NSLayoutConstraint!
    @IBOutlet weak var timeLine: UIView!
    @IBOutlet weak var dayField: UILabel!
    
    
    var delegate: EPGViewControllerDelegate?
    var channels : [Channel]?
    var epg : [ProgramItem]?
    private var channelEpg : [Int: [ProgramItem]] = [:] //Dictionary of Program Items for ID value
    private var currentTime = Date() //Start point for time scale on the top of table
    private let scaleWightFor1Minuts = 10.0 //Count of pixels on timeline need for size and position for placement each program item
    private let kROW_HEIGHT : CGFloat = 100.0
    private let dateFormatterISO = ISO8601DateFormatter()
    private let dateFormatter = DateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For test set the currentTime to first epg item time
        currentTime = dateFormatterISO.date(from: "2020-07-09T02:00:00Z")!
        
        //loading program guide
        delegate?.epgViewDidLoad()
    }
    
    
    //MARK: - Size & Position calculation
    private func calculatePosition(startTime: Date, duration: Int) -> CGRect {
        let startPoint = startTime.timeIntervalSince(currentTime) / 60 * scaleWightFor1Minuts
        let itemWight = Double(duration) * scaleWightFor1Minuts
        
        return CGRect(x: startPoint, y: 1, width: itemWight - 4, height: kROW_HEIGHT - 2)
    }
    
    
    //MARK: - TimeScale drawing
    private func timeScaleDraw() {
        guard epg?.count ?? 0 > 0 else { return }
        var maxTime = currentTime
        for item in epg! {
            let startTime = dateFormatterISO.date(from: item.startTime) ?? Date()
            let length = item.length
            let itemMaxTime = startTime.addingTimeInterval(Double(length) * 60)
            if itemMaxTime > maxTime {
                maxTime = itemMaxTime
            }
        }
        let scaleWidth = maxTime.timeIntervalSince(currentTime) / 60 * scaleWightFor1Minuts
        epgTableWidht.constant = scaleWidth
        
        //Draw timeline
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: scaleWidth, height: 54))
        let lineImage = renderer.image { ctx in
            ctx.cgContext.setStrokeColor(UIColor.white.cgColor)
            ctx.cgContext.setLineWidth(4)

            ctx.cgContext.move(to: CGPoint(x: 20, y: 50))
            ctx.cgContext.addLine(to: CGPoint(x: scaleWidth, y: 50))
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        let imageView = UIImageView(image: lineImage)
        timeLine.addSubview(imageView)
        
        //draw vertical time markers every 30 minuts
        let calendar = Calendar.current
        let currentMinute = calendar.component(.minute, from: currentTime)
        let firstIntervalTo30 = (currentMinute % 30) * Int(scaleWightFor1Minuts)
        let halfHourInterval = 30 * scaleWightFor1Minuts
        let markersCount = Int(scaleWidth / halfHourInterval)
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        
        for i in 1..<markersCount {
            let markImage = renderer.image { ctx in
                ctx.cgContext.setStrokeColor(UIColor.white.cgColor)
                ctx.cgContext.setLineWidth(4)
                let x = firstIntervalTo30 + (i * Int(halfHourInterval))
                ctx.cgContext.move(to: CGPoint(x: x, y: 50))
                ctx.cgContext.addLine(to: CGPoint(x: x, y: 20))
                ctx.cgContext.drawPath(using: .fillStroke)
                
                //draw text
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .left
                let attrs = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Thin", size: 25)!, NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor : UIColor.white]
                
                let time = currentTime.addingTimeInterval(Double(x) * 60 / scaleWightFor1Minuts)
                let string = dateFormatter.string(from: time)
                string.draw(with: CGRect(x: x + 8, y: 8, width: 150, height: 30), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
            }
            let markView = UIImageView(image: markImage)
            timeLine.addSubview(markView)
        }
        
        //Day field
        showDayInfo(date: currentTime)
    }
    
    
    //MARK: - Day Info
    private func showDayInfo(date: Date) {
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dayField.text = dateFormatter.string(from: date)
    }
}


//MARK: - TableView
extension EPGViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == channelsTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellChannel", for: indexPath) as! ChannelTableViewCell
            cell.picture.image = UIImage(named: "TV") //for example
            cell.number.text = "\(channels![indexPath.row].accessNum)"
            cell.name.text = "\(channels![indexPath.row].CallSign)"
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellEpg", for: indexPath) as! EpgTableViewCell
            cell.clearAllItems()
            
            let channelId = channels![indexPath.row].id
            let programItems = channelEpg[channelId] ?? []
            
            for item in programItems {
                guard let startDate = dateFormatterISO.date(from: item.startTime) else { continue }
                let itemFrame = calculatePosition(startTime: startDate, duration: item.length)
                let newView = ProgramItemView(frame: itemFrame)
                newView.nameString = item.name
                newView.programId = item.recentAirTime.id
                newView.delegate = self
                
                //translation time below the name
                dateFormatter.dateStyle = .none
                dateFormatter.timeStyle = .short
                newView.timeString = dateFormatter.string(from: startDate)
                
                cell.contentView.addSubview(newView)
            }
            
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kROW_HEIGHT
    }
}

extension EPGViewController: UITableViewDelegate {
    
}


//MARK: - Syncro scroll
//it's need for syncronicity left and right table scrolling
extension EPGViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != epgScrollView {
            channelsTable.contentOffset = scrollView.contentOffset
            epgTable.contentOffset = scrollView.contentOffset
        }
    }
}


//MARK: - Program Item delegate
extension EPGViewController: ProgramItemDelegate {
    func programItemIsFocused(id: Int) {
        if let dateString = epg?.first(where: { $0.recentAirTime.id == id })?.startTime,
            let date = dateFormatterISO.date(from: dateString) {
            showDayInfo(date: date)
        }
    }
}


//MARK: - EPG Loading
extension EPGViewController: EPGViewControllerInput {
    
    func putChannels(_ data: [Channel]) {
        channels = data
    }
    
    
    func putPrograms(_ data: [ProgramItem]) {
        epg = data
    }
    
    
    
    //MARK: - Arrange Programs
    func arrangeByChannels() {
        guard epg?.count ?? 0 > 0, channels?.count ?? 0 > 0 else { return }
        
        channelEpg.removeAll()
        for item in channels! {
            channelEpg[item.id] = epg!.filter { $0.recentAirTime.channelID == item.id }
        }
        
        timeScaleDraw()
        
        channelsTable.reloadData()
        epgTable.reloadData()
    }
}
