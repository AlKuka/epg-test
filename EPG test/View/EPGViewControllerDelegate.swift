//
//  EPGViewControllerDelegate.swift
//  EPG test
//
//  Created by Oleg Levkutnyk on 16.02.2023.
//

import Foundation

protocol EPGViewControllerDelegate {
    func channelDidSelect(id: Int)
    func programItemDidSelect(id: Int)
    func epgViewDidLoad()
}
